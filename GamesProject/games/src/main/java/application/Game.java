package application;

import Services.*;
import domain.Borrow;
import domain.Borrower;
import domain.Category;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;


import java.sql.SQLException;
import java.util.*;

import static java.lang.System.err;


public class Game {

    private Map<Integer, String> menuMap;
    private CategoryService categoryService;
    private GameService gameService;
    private DifficultyService difficultyService;
    private BorrowerService borrowerService;
    private BorrowService borrowService;

    private Scanner scanner;
    private boolean next = true;
    /**
     * Maakt een nieuw Game object aan. Dit zorgt voor het weergeven
     * van gegevens in de commandline en voor het uitvoeren van de opgevraagde
     * delen van het programma.
     *

     */

    public Game(CategoryService categoryService, GameService gameService, BorrowerService borrowerService, DifficultyService difficultyService, BorrowService borrowService) {

        this.scanner = new Scanner(System.in);
        this.categoryService = categoryService;
        this.gameService = gameService;
        this.difficultyService = difficultyService;
        this.borrowerService = borrowerService;
        this.borrowService = borrowService;
        //Initialise menu
        this.menuMap = new HashMap<>();
        menuMap.put(1, "Show category for id");
        menuMap.put(0, "quit the game");
        menuMap.put(2, "show game details for id");
        menuMap.put(3, "show borrower information by id");
        menuMap.put(4, "show games");
        menuMap.put(5, "show all games");
        menuMap.put(6, "show all games and choose one");
        menuMap.put(7, "show all borrowed items");
        menuMap.put(8, "advanced search games by");
        menuMap.put(9, "complex search borrowers");
        menuMap.put(10 ," searching in borrow");
        menuMap.put(11, "show all categories");
        menuMap.put(12, "Show games by page");
        menuMap.put(13, "insert new games in the DB");
    }
    /**
     * Start het spel op en toont een klein startscherm.
     */

    public void startGame() {
        System.out.println("---------------HELLO YONI----------------");
        System.out.println("Game options");
        showMenu();
    }
    /**
     * Geeft het menu weer.
     */

    private void showMenu() {
        for (Map.Entry<Integer, String> m : menuMap.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
        chooseOption();
    }
    /**
     * Biedt de mogelijkheid aan om het nummer van een optie in te geven.
     */

    private void chooseOption() {
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        try {
            System.out.print("Choose your option : ");
            int i = scanner.nextInt();
            executeOption(i);
        } catch (InputMismatchException e) {
            err.println("you need to gif me a number");


        } catch (NoRecordFoundException e) {
            e.printStackTrace();
            chooseOption();
        }
    }

    /**
     *     gebruikt de methode die je opgeeft bij input(Integer).
      */

    private void executeOption(int option) throws NoRecordFoundException {
        switch (option) {
            case 1:
                findCategory(option);
                break;

            case 2:
                findGame(5);
                break;
            case 3:
                findBorrowerSimpleInformation();
                break;
            case 4:
                showSelectGameID();
                break;
            case 5:
                showAllGames(1);
                break;
            case 6:
                showAllGamesAndShoosOne();
                showSelectGameID();

                break;
            case 7:

                showAllBorrows();

                break;
            case 8:
                int difficulty = askForDifficulty();
                showGamesFromMinDifficulty(difficulty);

                break;

            case 9:  String borrower = borrowerNameMatch();
                showBorrowersByNameStart(borrower);

                break;

          /*  case 10: ownWay();

            break;*/
            case 11 :ShowAllCategories();
              break;
            case 12 : showGamesByPage(1);
            break;
            case 13: insertIntoDb();
            case 0:
                closeGame();
                next = false;
                break;
        }if(next) {
      chooseOption();  }
scanner.close();


    }

    /**
     * sluit de game.
     */
    private void closeGame() {
        System.out.println("Closing the game");
    }

    /**
     * zoekt game op ID.
     * @param gameID
     */
    private void findGame(int gameID) {
        try {
            System.out.println("Showing game with ID " + gameID);
            domain.Game game = gameService.findGameByID(gameID);
            System.out.println(game.toString());
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    /**
     * geeft de borrower informatie weer.
     */
    private void findBorrowerSimpleInformation() {
        try {
            System.out.print("\nID of the borrower:");
            int id = scanner.nextInt();
            Borrower borrower = borrowerService.findBorrowerById(id);
            System.out.println("name: " + borrower.getName());
            System.out.println("city: " + borrower.getCity());
        } catch (IDOutOfBoundException | NoRecordFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Toont de ID en de naam van de categorie met de meegegeven ID .
     * @param option de ID van de te weergeven categorie.
     */

    private void findCategory(int option) {
        try {
            Category category = categoryService.findCategoryById(option);
            System.out.println(category.getId() + " ");
            System.out.println(category.getCategoryName());
        } catch (NoRecordFoundException e) {
            e.printUserFriendlyMessage();
            startGame();
        }
    }

    /**
     * Toont alle borrows.
     */
    private void showAllBorrows() {
        try {
            List<Borrow> borrows = borrowService.findAllBorrow();
            for (Borrow borrow : borrows) {
                System.out.println("[" + borrow.getId() + "]"+ " ** " + borrow.getBorrowerid() + " ** " + borrow.getBorrowDate() + "** " + borrow.getReturnDate() );
//Collections.sort(borrows);
            }} catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * geeft de informatie weer die je aanvraagt bij ID.
     * @return
     */
    private int showSelectGameID() {
        System.out.print("Insert the game ID: ");
        if (scanner.hasNextInt()) {
            int id = scanner.nextInt();
            try {
              domain.Game game =  gameService.findGameByID(id);
                System.out.println(game.getName() +  "  "+ game.getPrice()+ " "+ game.getAge()+ "  "+game.getAuthor());
            } catch (IDOutOfBoundException | NoRecordFoundException e) {
                e.printStackTrace();
            }

        } else {
            showSelectGameID();


        }
        return 0;
    }

    /**
     * Geeft een lijst van alle games weer.
     */
    private void showAllGamesAndShoosOne() {
        try {
            List<domain.Game> games = gameService.findAllGames();
            System.out.println(games + "ID\t\tGame name/Category");
            for (domain.Game game : games) {
                System.out.println( game.getId() + "  |\t" + game.getName() + "- " + game.getCategory().getCategoryName().toUpperCase());

            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Vraagt uit welke moeilijkheidsgraad je de games wil zien.
     * @return
     */
    private int askForDifficulty() {
        System.out.print("Enter the minimum difficulty [1 to 5]");
        if (scanner.hasNextInt()) {

            return scanner.nextInt();
        } else {
            askForDifficulty();
        }

        return 0;
    }



    /**
     *     Vraagt de gebruiker om een naam of het begin van een naam in te geven.
      */

    private String borrowerNameMatch() {
        scanner.nextLine();
        System.out.print("enter the name");
        if(scanner.hasNextLine()){
            return scanner.nextLine();
        } else {
            borrowerNameMatch();
        }
        return null;
    }

    /**
     * geeft de games weer die bij een bepaalde moeilijkheidsgraad horen.
     * @param difficulty
     */

    private void showGamesFromMinDifficulty(int difficulty) {
        System.out.println("--- Games ");
        try {
            List<domain.Game> gamen = gameService.findGameByDifficulty(difficulty);

            for (domain.Game game : gamen) {

                System.out.println("ID:" + game.getId());System.out.println("name:\t" + game.getName());System.out.println("publisher:\t" + game.getEditor());System.out.println("price:\t" + game.getPrice());

            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * geeft alle games weer.
     * @param gameId
     * @throws NoRecordFoundException
     */
    private void showAllGames(int gameId) throws NoRecordFoundException {

        List<domain.Game> games = gameService.findAllGames();

        System.out.println("----- ALL GAMES" );

        for (domain.Game game : games) {
            gameId++;
            System.out.println("Showing game with ID " + gameId);


           System.out.println("name :\t" + game.getName());System.out.println("publisher:\t" + game.getEditor());System.out.println("age:\t" + game.getAge());System.out.println("price:\t" + game.getPrice());

        }

    }

    /**
     * geeft een lijst van personen wie hun naam beginnen met de opgegeven characters.
     * @param starts
     */
    private void showBorrowersByNameStart(String starts) {
        System.out.println("Borrowers whose name stars with    " + starts);
        try {
           List<Borrower> borrowers = borrowerService.findBorrowersByNameStartingWith(starts);
        for (Borrower borrower1 : borrowers) {
            System.out.println(" BORROWER INFORMATION ");
            System.out.println("name:  " + borrower1.getName()+"email:  " + borrower1.getEmail() + "city:  " + borrower1.getCity()+"number:  " + borrower1.getTelephone() );

        }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * gestolen van maxime sorry
     * toont alle informatie bij elke pagina je wil wil zien.
     * @param page /
     */
    private void showGamesByPage(int page) {
        try{
            List<domain.Game> games = gameService.showGamePage(page);
            System.out.println("Page " + page );
            for(domain.Game game : games){
                System.out.println("id:\t" + game.getId()); System.out.println("name:\t" + game.getName()); System.out.println("publisher:\t" + game.getEditor()); System.out.println("price:\t\t" + game.getPrice());
            }
            int newPage = askForPage();
            if(newPage > 0){
                showGamesByPage(newPage);
            }

        } catch (NoRecordFoundException e) {
            System.out.println(" we couldn't find any more games ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * vraagt naar welke pagina je wil bekijken.
     * gestolen van maxime sorry
     * @return 0
     */
    private int askForPage() {
        System.out.print("load next page (-1 to exit): ");
        if(scanner.hasNextInt()){
            return scanner.nextInt();
        } else {
            askForPage();
        }
        return 0;
    }
  /*  private void ownWay() {

        Date date = scanner.ChooseDate();
        try {
            List<Borrow> borrows = borrowService.findAllBorrows(datum);
            for (Borrow borrow : borrows) {
                System.out.println("(" + borrow.getBorrower().getBorrowerName() + " \t" + borrow.getBorrowDate() + "\t" + borrow.getGame().getName() + ")");
            }
        } catch (NoRecordFoundException e) {
            e.printStackTrace();
           System.out.println(" your choosed option");
        }
*/

    /**
     * insert data in de DB.
     *
     */
    private void insertIntoDb() {
        System.out.println("ID?");
        int gameid =scanner.nextInt();
        scanner.nextLine();
        System.out.println(" game name?");
        String gamename = scanner.nextLine();
        try {
            List<domain.Game> games = gameService.findAllGames();

            for (domain.Game game : games) {
                System.out.println("[" + game.getId() + " \t" + game.getName() + "]");


            }  System.out.println(" insert into the DB?");
            String yes = scanner.nextLine();
            scanner.nextLine();
            if (yes.equals("yes")) {
                gameService.insertIntoGames(gameid,gamename);
            }else {
                return;
            }
        } catch (IllegalAccessException | NoRecordFoundException e) {
            e.printStackTrace();
        }
    }



    /**
     * Zoekt naar alles in category en geeft id en naam weer.
     * @throws NoRecordFoundException
     */

    private void ShowAllCategories() throws NoRecordFoundException {
        List<Category> categories = categoryService.findAllCategories();
        System.out.println("ALL CATEGORIES");
        for (domain.Category categorie : categories) {
            System.out.println(categorie.getId()+"\t" + categorie.getCategoryName());
        }
    }}