package repo;

import
domain.Difficulty;
import utilities.DataBaseUtil;

import java.sql.*;
public class DifficultyDAO {

    public static Difficulty difficultyById(int id){
        Difficulty difficulty = new Difficulty();
/**
 * connectie maken met de DB en een query maken voor de difficulty ID  uit de DB te nemen.
 */
        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            PreparedStatement s = con.prepareStatement("SELECT * FROM difficulty WHERE id = ?");
            s.setInt(1, id);
            s.execute();
            ResultSet result = s.getResultSet();
            if(result.next()){
                difficulty = new Difficulty();
                difficulty.setId(result.getInt("id"));
                difficulty.setDifficulty(result.getString("difficulty_name"));
            } s.close();
            con.close();


        } catch (SQLException e) {
            e.printStackTrace();

        }
        return difficulty;
    }
}


