package repo;

import domain.Borrower;
import utilities.DataBaseUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static repo.CategoryDAO.USERNAME;
import static utilities.DataBaseUtil.URL;

public class BorrowerDAO {
    /**
     * Zoekt borrower bij ID.
     * @param id een integer meegeven voor de juiste borrower te vinden.
     * @return borrower
     */
    public Borrower findBorrowerById(int id) {
        Borrower borrower = new Borrower();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, "");
            statement = connection.prepareStatement("SELECT * FROM borrower WHERE id = ?");
            statement.setInt(1, id);
            statement.execute();
            ResultSet set = statement.getResultSet();
            if (set.next()) {
                borrower = createObjectFromSQL(set);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return borrower;
    }

    /**
     * Maakt een object aan in de DB.
     * @param rs /
     * @return borrower
     */
        private Borrower createObjectFromSQL(ResultSet rs) {
         Borrower borrower = new Borrower();
        try {
            borrower.setId(rs.getInt("id"));
            borrower.setName(rs.getString("borrower_name"));
            borrower.setStreet(rs.getString("street"));
            borrower.setHouseNumber(rs.getString("house_number"));
            borrower.setBusNumber(rs.getString("bus_number"));
            borrower.setPostcode(rs.getInt("postcode"));
            borrower.setCity(rs.getString("city"));
            borrower.setTelephone(rs.getString("telephone"));
            borrower.setEmail(rs.getString("email"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return borrower;
    }

    /**
     * Zoekt naar alle borrowers
     * @return borrowers
     */
    public List<Borrower> findAllBorrowers() {
        List<Borrower> borrowers = new ArrayList<>();

        try {
            Connection con = DriverManager.getConnection(URL, USERNAME, "");


            PreparedStatement s = con.prepareStatement( "SELECT * FROM borrower");

            s.execute();

            ResultSet rs = s.getResultSet();
            while(rs.next()){
                Borrower borrower ;
                borrower = createObjectFromSQL( rs);
                borrowers.add(borrower);

            }
            s.close();
            con.close();
            return borrowers;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return borrowers;
    }

    /**
     * Zoekt naar borrowers waarvan hun naam beginnen met de opgegeven input.
     * @param starts
     * @return borrowers
     */
    public List<Borrower> findBorrowersStartingWith(String starts){
        List<Borrower> borrowers = new ArrayList<>();

        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");

            PreparedStatement s = con.prepareStatement("SELECT * FROM borrower WHERE borrower_name LIKE ? ORDER BY borrower_name");
           s.setString(1,starts+ "%");
            s.execute();
            ResultSet rs = s.getResultSet();

            while(rs.next()){
                Borrower borrower ;
                borrower = createObjectFromSQL( rs);
                borrowers.add(borrower);
            }
            s.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return borrowers;
    }


}



