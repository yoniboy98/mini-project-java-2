package repo;

import Services.CategoryService;
import Services.DifficultyService;
import domain.Category;
import domain.Difficulty;
import domain.Game;
import exceptions.NoRecordFoundException;
import utilities.DataBaseUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GameDAO {

    /**
     * Zoekt game bij ID.
     * @param id Zoekt naar game op ID.
     * @return game
     */
    public Game findGameById(int id) {
        Game game = null;
        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            PreparedStatement s = con.prepareStatement("SELECT * FROM game WHERE id = ?");

            s.setInt(1, id);
            s.execute();
            ResultSet results = s.getResultSet();

            if (results.next()) {
                game = new domain.Game();
                game.setId(results.getInt("id"));
                game.setName(results.getString("game_name"));
                game.setEditor(results.getString("editor"));
                game.setAuthor(((ResultSet) results).getString("author"));
                game.setYear(results.getInt("year_edition"));
                game.setAge(results.getString("age"));
                game.setMinimumPlayers(results.getInt("min_players"));
                game.setMaximumPlayers(results.getInt("max_players"));

                CategoryDAO categoryDAO = new CategoryDAO();
                CategoryService categoryService = new CategoryService(categoryDAO);
                Category c = categoryService.findCategoryById(results.getInt("category_id"));

                game.setCategory(c);
                game.setDuration(results.getString("play_duration"));

            }

        } catch (SQLException | NoRecordFoundException e) {
            e.printStackTrace();
        }
        return game;
    }

    /**
     * Zoekt naar de games bij opgegeven moeilijkheid.
     * @param difficulty
     * @return games
     */
    public List<Game> findGameByMinimumDifficulty(int difficulty) {
        List<Game> games = new ArrayList<>();


        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");

            String query = "SELECT * FROM game WHERE difficulty_id >= ?";
            PreparedStatement s = con.prepareStatement(query);
            s.setInt(1, difficulty);
            s.execute();
            ResultSet rs = s.getResultSet();

            while (rs.next()) {
                Game game ;
                game = createObjectFromSQL( rs);
                games.add(game);
            }
            s.close();
            con.close();

        } catch (SQLException | NoRecordFoundException e) {
            e.printStackTrace();
        }
        return games;
    }

    /**
     * Maakt een object aan.
     * @param rs
     * @return game
     * @throws SQLException
     * @throws NoRecordFoundException
     */

    private Game createObjectFromSQL( ResultSet rs) throws SQLException, NoRecordFoundException {
           Game game = new Game();
            try {
                game.setId(rs.getInt("id"));
                game.setName(rs.getString("game_name"));
                game.setEditor(rs.getString("editor"));
                game.setAuthor(rs.getString("author"));
                game.setYear(rs.getInt("year_edition"));
                game.setAge(rs.getString("age"));
                game.setMinimumPlayers(rs.getInt("min_players"));
                game.setMaximumPlayers(rs.getInt("max_players"));

                game.setPrice(rs.getInt("price"));
                game.setImage(rs.getString("image"));

                if (rs.getInt("category_id") == 0) {
                    game.setCategory(null);
                } else {
                    CategoryDAO categoryDAO = new CategoryDAO();
                    CategoryService categoryService = new CategoryService(categoryDAO);
                    Category cat = categoryService.findCategoryById(rs.getInt("category_id"));
                    game.setCategory(cat);

                    if (rs.getInt("difficulty_id") == 0) {
                        game.setDifficulty(null);
                    } else {
                        DifficultyDAO difficultyDAO = new DifficultyDAO();
                        DifficultyService difficultyService = new DifficultyService(difficultyDAO);
                        Difficulty dif = difficultyService.difficultyById(rs.getInt("difficulty_id"));
                        game.setDifficulty(dif);
                    }


                }
            } catch (SQLException | NoRecordFoundException e) {
                e.printStackTrace();
            }
        return game;
        }


    /**
     * Zoekt naar alle games in de DB.
     * @return games
     */
    public List<Game> findAllGames() {
        List<Game> games = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            PreparedStatement s = con.prepareStatement("SELECT * FROM game");

            s.execute();
            ResultSet results = s.getResultSet();
            while(results.next()) {
                Game game;
                game = createObjectFromSQL(results);
                games.add(game);
            }
            System.out.println(results);
            results.close();
            s.close();

        } catch (SQLException | NoRecordFoundException e) {
            e.printStackTrace();
        }
        return games;
    }

    /**
     * Gestolen van maxime .
     *
     * @param page
     * @return games
     * @throws SQLException
     */
    public List<Game> showGamesFromPage(int page) throws SQLException {
        List<Game> games = new ArrayList<>();
        int offset = page * 5-5;
        Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
        try {

            String query = "SELECT * FROM game" +
                    "       ORDER BY game_name " +
                    "       LIMIT 5 OFFSET " + offset;
            PreparedStatement s = con.prepareStatement(query);
            s.execute();
            ResultSet rs = s.getResultSet();

            while(rs.next()){
                Game game ;
                game = createObjectFromSQL( rs);

                games.add(game);
            }
            s.close();
            s.close();
        } catch (SQLException | NoRecordFoundException e) {
            e.printStackTrace();
        }
        return games;
    }

    /**
     * Maakt een query voor een insert te doen in games.
     * @param game_id
     * @param game_name
     */
    public void insertIntoGame(int game_id, String game_name) {

        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            PreparedStatement  statement = con.prepareStatement("INSERT INTO `games`.`game`(`id`,`game_name`,`editor`,`author`,`year_edition`,`age`,`min_players`,`max_players`,`category_id`,`price`)VALUES\n" +
                    "(?,?,?,?,?,?,?,?,?,?);\n" +
                    "\n");
            statement.setInt(1, game_id);
            statement.setString(2, game_name);
            statement.setString(3,"de helix");
            statement.setString(4,"kies maar");
            statement.setInt(5,2018);
            statement.setInt(6, 8);
            statement.setInt(7,1);
            statement.setInt(8,2);
            statement.setInt(9,4);
            statement.setInt(10, 4);


            statement.executeUpdate();
            System.out.println("succesfully");
            con.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
