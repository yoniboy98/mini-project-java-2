package repo;

import domain.Category;
import utilities.DataBaseUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoryDAO {

    private static final String URL = "jdbc:mysql://localhost:3306/games";
    protected static final String USERNAME = "root";

    /**
     * Zoekt categorie bij ID
     * @param id
     * @return category
     */
    public Category findCategoryById(int id) {
        Category category = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet set = null;
        try {
            connection = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            preparedStatement = connection.prepareStatement("SELECT * FROM category WHERE id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            set = preparedStatement.getResultSet();
            while (set.next()) {
                category = new Category();
                category.setId(set.getInt("id"));
                category.setCategoryName(set.getString("category_name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            try {
                set.close();
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return category;
    }

    /**
     * Zoekt categorie bij naam.
     * @param begin Geeft een string weer waamree de naam gepast moet beginnen.
     * @return categories
     */
    public List<Category> findCategoryByName(String begin) {
        List<Category> categories = new ArrayList<Category>();
        try {

            Connection con = DriverManager.getConnection(URL, USERNAME, "");
            PreparedStatement statement = con.prepareStatement("SELECT * FROM category WHERE category_name LIKE ?");
            statement.setString(1, begin + "%");
            statement.execute();
            ResultSet set = statement.getResultSet();
            while (set.next()) {
                Category category = new Category();
                category.setCategoryName(set.getString("category_name"));
                category.setId(set.getInt("id"));
                categories.add(category);
            }
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categories;
    }

    /**
     * Zoekt naar alle data in category.
     * @return
     */
    public List<Category> findAllCategories() {
        List<Category> cat = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            PreparedStatement s = con.prepareStatement("SELECT * FROM games.category");

            s.execute();
            ResultSet results = s.getResultSet();
            while(results.next()) {
                Category category = new Category();
                category.setCategoryName(results.getString("category_name"));
                category.setId(results.getInt("id"));
                cat.add(category);
            }
            System.out.println(results);
            results.close();
            s.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cat;
    }
}
