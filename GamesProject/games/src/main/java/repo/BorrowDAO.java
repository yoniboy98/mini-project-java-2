package repo;

import domain.Borrow;
import utilities.DataBaseUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Zoekt naar alle borrows in de column.
 */
public class BorrowDAO {

    public List<Borrow> findAllBorrows() {
        List<Borrow> borrows = new ArrayList<>();

        try {

            Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            PreparedStatement s = con.prepareStatement("SELECT id, game_id, borrower_id, borrow_date, return_date FROM `games`.`borrow`");
            s.execute();

            ResultSet rs = s.getResultSet();

            while (rs.next()) {

                Borrow borrow = createObjectFromSQL(new Borrow(), rs);

                borrows.add(borrow);
            }
            con.close();
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return borrows;
    }


    /**
     * insert game id en borrower id in DB.
     *
     * @param
     */

 /*   public List<Borrow> findBorrowsDate(Date datum) {
        List<Borrow> borrows = new ArrayList<>();
        java.sql.Date dateSql = new java.sql.Date(datum.getTime());
        try {
           Connection con = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
             PreparedStatement statement = con.prepareStatement("SELECT * FROM games.borrow WHERE borrow_date<=?");
            statement.setString(1, String.valueOf(dateSql));
            statement.execute();

            set = statement.getResultSet();
            while (set.next()) {
                Borrow borrow = aggregateBorrow();
                borrows.add(borrow);
            }
            return borrows;
        } catch (SQLException e) {
            e.printStackTrace();

      */

    /**
     * Maakt een object aan
     * @param borrow
     * @param rs
     * @return borrow
     * @throws SQLException
     */
    private Borrow createObjectFromSQL(Borrow borrow, ResultSet rs) throws SQLException {
        borrow.setId(rs.getInt("id"));

        borrow.setBorrowerid(rs.getInt("borrower_id"));
        borrow.setBorrowDate(rs.getDate("borrow_date"));
        borrow.setReturnDate(rs.getDate("return_date"));

        if (rs.getInt("game_id") == 0) {
            borrow.setGame(null);

        }
        return borrow;
    }
}