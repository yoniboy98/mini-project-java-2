import Services.*;
import application.Game;
import repo.*;

public class GamesApp {



    public static void main(String[] args) {
        CategoryDAO categoryDAO = new CategoryDAO();
        CategoryService categoryService = new CategoryService(categoryDAO);

        GameDAO gameDAO = new GameDAO();
        GameService gameService = new GameService(gameDAO);

        DifficultyDAO difficultyDAO = new DifficultyDAO();
        DifficultyService difficultyService = new DifficultyService(difficultyDAO);

        BorrowerDAO borrowerDAO = new BorrowerDAO();
        BorrowerService borrowerService = new BorrowerService(borrowerDAO);

        BorrowDAO borrowDAO = new BorrowDAO();
        BorrowService borrowService = new BorrowService(borrowDAO);

        Game game = new Game(categoryService, gameService, borrowerService, difficultyService, borrowService);

        game.startGame();

    }
}
