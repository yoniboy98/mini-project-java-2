package exceptions;

public class IDOutOfBoundException extends Exception{
    public IDOutOfBoundException(String message) {
        super(message);
    }
}
