package domain;

import java.util.*;

public class Borrower implements List<Borrower> {
    private int id;
    private String name;
    private String street;



    private String houseNumber;
    private String  busNumber;
    private int postcode;
    private String city;
    private String telephone;
    private String email;
    private Game game;
    private Borrower borrower;
    private Date BorrowDate;
    private Date returnDate;

    public Borrower() {}

    public Borrower(int id, String name, String street, String houseNumber, String busNumber, int postcode, String city, String telephone, String email, Game game , Borrower borrower , Date BorrowDate,Date returnDate) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.houseNumber = houseNumber;
        this.busNumber = busNumber;
        this.postcode = postcode;
        this.city = city;
        this.telephone = telephone;
        this.email = email;
        this.game = game;
        this.borrower = borrower;
        this.BorrowDate = BorrowDate;
        this.returnDate = returnDate;


    }

    @Override
    public String toString() {
        return "Borrower{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", street='" + street + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", busNumber='" + busNumber + '\'' +
                ", postcode=" + postcode +
                ", city='" + city + '\'' +
                ", telephone='" + telephone + '\'' +
                ", email='" + email + '\'' + ", game=" + game +
                ", borrower=" + borrower +
                ", borrowDate=" + BorrowDate +
                ", returnDate=" + returnDate +

                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber(int number) {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getBusNumber() {
        return busNumber;
    }

    public void setBusNumber(String busNumber) {
        this.busNumber = busNumber;
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Borrower getBorrower() {
        return borrower;
    }

    public void setBorrower(Borrower borrower) {
        this.borrower = borrower;
    }

    public Date getBorrowDate() {
        return BorrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        BorrowDate = borrowDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<Borrower> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Borrower borrower) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Borrower> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Borrower> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public Borrower get(int index) {
        return null;
    }

    @Override
    public Borrower set(int index, Borrower element) {
        return null;
    }

    @Override
    public void add(int index, Borrower element) {

    }

    @Override
    public Borrower remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Borrower> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Borrower> listIterator(int index) {
        return null;
    }

    @Override
    public List<Borrower> subList(int fromIndex, int toIndex) {
        return null;
    }
}


