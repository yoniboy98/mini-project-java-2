package domain;

public class Game {
    private int id;
    private String name;
    private String editor;
    private String author;
    private int year;
    private String age;
    private int minimumPlayers;
    private int maximumPlayers;
    private Category category;
    private String duration;
    private Difficulty difficulty;
    private float price;
    private String image;

    public Game(int id,
                String name,
                String editor,
                String author,
                int year,
                String age,
                int minimumPlayers,
                int maximumPlayers,
                Category category,
                String duration,
                Difficulty difficulty,
                float price,
                String image) {
        this.id = id;
        this.name = name;
        this.editor = editor;
        this.author = author;
        this.year = year;
        this.age = age;
        this.minimumPlayers = minimumPlayers;
        this.maximumPlayers = maximumPlayers;
        this.category = category;
        this.duration = duration;
        this.difficulty = difficulty;
        this.price = price;
        this.image = image;
    }

    public Game() {

    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", editor='" + editor + '\'' +
                ", author='" + author + '\'' +
                ", year=" + year +
                ", age='" + age + '\'' +
                ", minimumPlayers=" + minimumPlayers +
                ", maximumPlayers=" + maximumPlayers +
                ", category=" + category +
                ", duration='" + duration + '\'' +
                ", difficulty=" + difficulty +
                ", price=" + price +
                ", image='" + image + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public int getMinimumPlayers() {
        return minimumPlayers;
    }

    public void setMinimumPlayers(int minimumPlayers) {
        this.minimumPlayers = minimumPlayers;
    }

    public int getMaximumPlayers() {
        return maximumPlayers;
    }

    public void setMaximumPlayers(int maximumPlayers) {
        this.maximumPlayers = maximumPlayers;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}


