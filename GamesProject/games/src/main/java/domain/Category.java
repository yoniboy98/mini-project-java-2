package domain;

public class Category {

    private int id;
    private String categoryName;


    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "Category : id " + id + " name " + categoryName;
    }

    public int getId() {
        return id;
    }
}