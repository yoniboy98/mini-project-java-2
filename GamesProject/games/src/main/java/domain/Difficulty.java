package domain;
public class Difficulty  {

    private int id;
    private String difficulty;

    public Difficulty(int id, String difficulty) {
        this.id = id;
        this.difficulty = difficulty;
    }

    public Difficulty() { }

    @Override
    public String toString() {
        return "Difficulty{" +
                "id=" + id +
                ", difficulty='" + difficulty + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }
}

