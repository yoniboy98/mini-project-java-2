package domain;

import java.util.Date;

public class Borrow  {
    private int id;
    private Game game;
    private int borrowerid;
    private Date borrowDate;
    private Date returnDate;


    public void setGame(Game game) {
        this.game = game;
    }

    public void setBorrowerid(int borrowerid) {
        this.borrowerid = borrowerid;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public int getBorrowerid() {
        return borrowerid;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }


    public Borrow() {
    }

    @Override
    public String toString() {
        return "Borrow{" +
                "id=" + id +
                ", game=" + game +
                ", borrower=" + borrowerid +
                ", borrowDate=" + borrowDate +
                ", returnDate=" + returnDate +
                '}';
    }


}
