package Services;

import domain.Game;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import repo.GameDAO;

import java.sql.SQLException;
import java.util.List;

public class GameService {
    private GameDAO gameDAO;

    public GameService(GameDAO gameDAO) {
        this.gameDAO = gameDAO;
    }

    /**
     * Zoekt een spel op ID.
     *
     * @param id
     * @return game
     * @throws IDOutOfBoundException
     * @throws NoRecordFoundException
     */
    public Game findGameByID(int id) throws IDOutOfBoundException, NoRecordFoundException {
        if (id <= 0) {
            throw new IDOutOfBoundException("no legit ID!");
        } else {
            Game game = gameDAO.findGameById(id);
            if (game == null) {
                throw new NoRecordFoundException(" no games ");
            } else {
                return game;
            }

        }
    }

    /**
     * @param difficulty
     * @return Zoekt alle spellen die minstens de opgegeven Difficulty hebben.
     * @throws NoRecordFoundException
     */
    public List<Game> findGameByDifficulty(int difficulty) throws NoRecordFoundException {
        if (difficulty <= 0 || difficulty > 5) {
            throw new IllegalArgumentException();
        } else {
            List<Game> games = gameDAO.findGameByMinimumDifficulty(difficulty);
            if (games.isEmpty()) {
                throw new NoRecordFoundException("no games found ");
            } else {
                return games;
            }
        }
    }

    /**
     * Zoekt naar alle games.
     *
     * @return
     * @throws NoRecordFoundException
     */
    public List<Game> findAllGames() throws NoRecordFoundException {
        if (!gameDAO.findAllGames().isEmpty()) {
            return gameDAO.findAllGames();
        } else {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }


    }

    /**
     * Zoekt alle spellen die op de opgegeven pagina behoren.
     *
     * @param page
     * @return
     * @throws NoRecordFoundException
     * @throws
     */
    public List<Game> showGamePage(int page) throws NoRecordFoundException, SQLException {
        if (page <= 0) {
            throw new IllegalArgumentException();
        } else {
            List<Game> games = gameDAO.showGamesFromPage(page);
            if (games.isEmpty()) {
                throw new NoRecordFoundException("no, there are no games ");
            } else {
                return games;
            }
        }
    }

    public void insertIntoGames(int game_id, String game_name) throws IllegalAccessException {
        if (game_id < 0) {
            throw new IllegalAccessException("no legit NUMBER!");
        } else {
             gameDAO.insertIntoGame(game_id, game_name);
        }



    }

}