package Services;


import domain.Borrow;
import exceptions.NoRecordFoundException;
import repo.BorrowDAO;

import java.util.List;

public class BorrowService {
    private BorrowDAO borrowDAO;

    public BorrowService(BorrowDAO borrowDAO) {
        this.borrowDAO = borrowDAO;
    }

    /**
     * Zoekt alle verhuurde spellen en bij wie  zijn ID alsook de ID van de
     *       huur, de huurdatum, retourdatum, naam van de verhuurder.
     * @return borrows
     * @throws NoRecordFoundException Moest borrow leeg zijn.
     */
    public List<Borrow> findAllBorrow() throws NoRecordFoundException {
        List<Borrow> borrow = borrowDAO.findAllBorrows();

        if (borrow.isEmpty()) {
            throw new NoRecordFoundException("");
        }
        return borrow;
    }
    }





  /* public List<Borrow> findABorrowDate(Date datum) throws NoRecordFoundException {
        List<Borrow> borrow = borrowDAO.findBorrowsDate((java.sql.Date) datum);
        if (borrow == null || borrow.equals("[]") || borrow.isEmpty()) {
            throw new NoRecordFoundException("Ooops, there went something wrong");
        }
        return borrow;
*/







