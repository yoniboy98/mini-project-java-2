package Services;

import domain.Category;
import exceptions.NoRecordFoundException;
import repo.CategoryDAO;

import java.util.List;

public class CategoryService {

    private CategoryDAO categoryDAO;

    /**
     * Maakt een nieuw CategoryService object aan.
     * @param categoryDAO
     */
    public CategoryService(CategoryDAO categoryDAO){
        this.categoryDAO = categoryDAO;
    }

    /**
     * Zoekt naar de category bij de juiste ID.
     * @param id Zoekt de categorienaam op ID.
     * @return category
     * @throws NoRecordFoundException
     */
    public Category findCategoryById(int id) throws NullPointerException , NoRecordFoundException {
        Category category = categoryDAO.findCategoryById(id);
        if (category==null){
            throw new NoRecordFoundException("no record with this ID in our categories.");
        }
        return category;
    }


   /* public List<Category> findCategoryByName(String match) throws NoRecordFoundException {
        List<Category> matcht =  categoryDAO.findCategoryByName(match);
        if(matcht.isEmpty() ){
            throw new NoRecordFoundException("something went wrong here " + match);
        } else {
            return matcht;
        }
    }*/

    /**
     * Zoekt naar alle categorieen.
     * @throws NoRecordFoundException
     */
    public List<Category> findAllCategories() throws NoRecordFoundException {
        if (!categoryDAO.findAllCategories().isEmpty()) {
            return categoryDAO.findAllCategories();
        } else {
            throw new NoRecordFoundException("We're sorry, but we have no record of what you tried to look up");
        }
    }
}

