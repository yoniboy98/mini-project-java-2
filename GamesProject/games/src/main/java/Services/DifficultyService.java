package Services;

import domain.Difficulty;
import repo.DifficultyDAO;

public class DifficultyService {
    private DifficultyDAO difficultyDAO;


    public DifficultyService(DifficultyDAO difficultyDAO) {
        this.difficultyDAO = difficultyDAO;
    }

    /**
     * Zoekt difficulty ID.
     * @param id
     * @return difficulty
     */
    public Difficulty difficultyById(int id){
        Difficulty difficulty = DifficultyDAO.difficultyById(id);
        if ( difficulty == null) {
            throw new NullPointerException();
        } else {
            return difficulty;
        }

    }


}

