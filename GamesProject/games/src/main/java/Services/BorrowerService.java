package Services;

import domain.Borrower;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import repo.BorrowerDAO;

import java.util.List;

public class BorrowerService {


    private BorrowerDAO borrowerDAO;

    public BorrowerService(BorrowerDAO dao) {
        this.borrowerDAO = dao;
    }

    /**
     * Zoekt naar de borrower met de juiste ID.
     * @param id een integer meegeven voor de juiste borrower te vinden.
     * @return borrower
     * @throws IDOutOfBoundException Moest de id kleiner of gelijk zijn aan 0.
     * @throws NoRecordFoundException Moest borrower geljk zijn aan null.
     */
    public Borrower findBorrowerById(int id) throws IDOutOfBoundException, NoRecordFoundException {
        if (id <= 0) {
            throw new IDOutOfBoundException("no legit number");
        } else {
            Borrower borrower = borrowerDAO.findBorrowerById(id);

            if (borrower == null) {
                throw new NoRecordFoundException("no legit number");
            } else {
                return borrower;
            }
        }

    }


    /**
     * Zoekt naar alle borrows.
     * @return borrows
     * @throws NoRecordFoundException
     *
     */
    public List<Borrower> findAllBorrows() throws NoRecordFoundException {

        List<Borrower> borrows = borrowerDAO.findAllBorrowers();
        if (borrows.isEmpty()) {
            throw new NoRecordFoundException("");
        } else {
            return borrows;
        }


    }

    /**
     * Zoekt naar de borrows waar de naam begint met de gegeven string.
     * @param starts Zoekt naar de 1ste char die je ingeeft en zoekt dan alle matchende namen.
     * @return borrowers
     * @throws NoRecordFoundException
     */
    public List<Borrower> findBorrowersByNameStartingWith(String starts) throws NoRecordFoundException {
        List<Borrower> borrowers;
        borrowers = borrowerDAO.findBorrowersStartingWith(starts);
        if (borrowers.isEmpty()) {
            throw new NoRecordFoundException("");
        } else {
            return borrowers;
        }
    }
}
