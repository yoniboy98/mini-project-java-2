package RepositoryTest;

import domain.Game;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import repo.GameDAO;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class GameDaoTest {
    private Connection connection;
    @BeforeEach
    public void connection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/gamesTest", DataBaseUtil.USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testMinimumDifficulty(){
        GameDAO dao = new GameDAO();
        List<Game> results = dao.findGameByMinimumDifficulty(1);

        Assertions.assertFalse(results.isEmpty());

    }

    @Test
    public void testFindAllGames(){
        GameDAO dao = new GameDAO();
        List<domain.Game> result = dao.findAllGames();
        Assertions.assertFalse(result.isEmpty());
    }

    @Test
    public void testShowGamesFromPage() throws SQLException {
        GameDAO game = new GameDAO();
        List<domain.Game> results = game.showGamesFromPage(5);

        Assertions.assertFalse(results.isEmpty());
        Assertions.assertEquals(5, results.size());
    }
    @Test
public void testFindGameById(){
        GameDAO game = new GameDAO();
        Game result = game.findGameById(1);
        Assertions.assertEquals(1 , result.getId());



}

}
