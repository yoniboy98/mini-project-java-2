package RepositoryTest;

import domain.Difficulty;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import repo.DifficultyDAO;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DifficultyDaoTest {

    private Connection connection;

    @BeforeEach
    public void connection() {
        try {
            connection = DriverManager.getConnection(DataBaseUtil.URL,DataBaseUtil.USERNAME,"");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testDifficultyById() {

        Difficulty result = DifficultyDAO.difficultyById(1);
        Assertions.assertEquals("Difficulty{id=1, difficulty='very easy'}", result.toString());
    }


}
