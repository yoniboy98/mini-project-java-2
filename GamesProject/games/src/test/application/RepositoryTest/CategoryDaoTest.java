package RepositoryTest;

import domain.Category;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import repo.CategoryDAO;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CategoryDaoTest {
private Connection connection;

        @BeforeEach
        public void connection() {
            try {
             connection  = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
            } catch (SQLException e) {
                e.printStackTrace();
            }}


        @Test
        public void testFindCategoryByID(){
            CategoryDAO dao = new CategoryDAO();
            Category result = dao.findCategoryById(1);
            assertEquals("combination", result.getCategoryName());
        }

        @Test
        public void testfindCategoryByName(){
            CategoryDAO dao = new CategoryDAO();
            List<Category> result = dao.findCategoryByName("combination");
            Assertions.assertFalse(result.isEmpty());
        }


     }


