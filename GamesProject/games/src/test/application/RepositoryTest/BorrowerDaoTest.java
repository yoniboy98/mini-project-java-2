package RepositoryTest;

import domain.Borrower;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import repo.BorrowerDAO;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class BorrowerDaoTest {
private Connection connection;
    @BeforeEach
    public void connection() {
        try {
            connection  = DriverManager.getConnection("jdbc:mysql://localhost:3306/gamesTest", DataBaseUtil.USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testFindBorrowerByID() {
        BorrowerDAO test = new BorrowerDAO();
        Borrower result = test.findBorrowerById(16);
        Assertions.assertEquals("Toon Huybrechts", result.getName());
    }

    @Test
    public void testFindBorrowersStartingWith() {
        BorrowerDAO test = new BorrowerDAO();
        List<Borrower> results = test.findBorrowersStartingWith("Jan Peeter");
        Assertions.assertFalse(results.isEmpty());


}
@Test
public void testFindAllBorrowers(){
        BorrowerDAO test =new BorrowerDAO();
        List<Borrower> result = test.findAllBorrowers();
        Assertions.assertFalse(result.isEmpty());
        }
}
