package RepositoryTest;

import domain.Borrow;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import repo.BorrowDAO;
import utilities.DataBaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class BorrowDaoTest {

    private Connection connection;
    @BeforeEach
    public void connection(){
        try {
            connection = DriverManager.getConnection(DataBaseUtil.URL, DataBaseUtil.USERNAME, "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFindABorrow(){
        BorrowDAO dao = new BorrowDAO();
        List<Borrow> results = dao.findAllBorrows();
        Assertions.assertFalse(results.isEmpty());

    }


}


