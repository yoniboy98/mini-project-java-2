package ServicesTest;

import Services.BorrowService;
import domain.Borrow;
import exceptions.NoRecordFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.BorrowDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class BorrowServiceTest {

    @Mock
    BorrowDAO borrowDAO;

    @InjectMocks
    BorrowService borrowService;

   @Test

    public void testFindABorrow() throws NoRecordFoundException {
        List<Borrow> borrows = new ArrayList<>();
        borrows.add(new Borrow());
        Mockito.when(borrowDAO.findAllBorrows()).thenReturn(borrows);
        List<Borrow> result = borrowService.findAllBorrow();
        verify(borrowDAO, times(1)).findAllBorrows();
        assertEquals(borrows, result);
    }

@Test
    public void testNoRecordFoundException() {
    Assertions.assertThrows(NoRecordFoundException.class,()-> {
        borrowService.findAllBorrow();
});
}


}
