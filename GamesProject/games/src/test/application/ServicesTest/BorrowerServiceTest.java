package ServicesTest;

import Services.BorrowerService;
import domain.Borrower;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.BorrowerDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class BorrowerServiceTest {
    @Mock
    BorrowerDAO dao;

    @InjectMocks
    BorrowerService service;
@Test
    public void testFindBorrowerById() throws NoRecordFoundException, IDOutOfBoundException {
        Borrower borrower = new Borrower();

        when(dao.findBorrowerById(1)).thenReturn(borrower);
        Borrower result = service.findBorrowerById(1);
        verify(dao, times(1)).findBorrowerById(anyInt());
        assertEquals(borrower, result);

    }
    @Test
    public void testFindAllBorrowers() throws NoRecordFoundException {
        List<Borrower> borrowers = new ArrayList<>();
        borrowers.add(new Borrower());

            when(dao.findAllBorrowers()).thenReturn(borrowers);
            List<Borrower> results = service.findAllBorrows();
            verify(dao, times(1)).findAllBorrowers();
            assertEquals(borrowers, results);


    }
    @Test
    public void testFindBorrowersStartingWith() throws NoRecordFoundException {
        List<Borrower> borrowers = new ArrayList<>();
        borrowers.add(new Borrower());

            when(dao.findBorrowersStartingWith("Toon")).thenReturn(borrowers);
            List<Borrower> results = service.findBorrowersByNameStartingWith("Toon");
            verify(dao, times(1)).findBorrowersStartingWith(anyString());
            assertEquals(borrowers, results);

    }
    @Test
    public void testAllBorrowsNoRecordFound() throws NoRecordFoundException {
Assertions.assertThrows(NoRecordFoundException.class,() -> {
            List<Borrower> borrowers = new ArrayList<>();
            when(dao.findAllBorrowers()).thenReturn(borrowers);
            service.findAllBorrows();
        });
    }
        @Test
        public void testSearchByIDOutOfBound() {
            Assertions.assertThrows(IDOutOfBoundException.class, () -> {
                service.findBorrowerById(-1);

            });
        }



}

