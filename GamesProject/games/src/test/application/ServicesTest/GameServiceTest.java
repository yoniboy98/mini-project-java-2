package ServicesTest;

import Services.GameService;
import domain.Game;
import exceptions.IDOutOfBoundException;
import exceptions.NoRecordFoundException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.GameDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {
    @Mock
    private GameDAO gameDAO;

    @InjectMocks
    GameService gameService;

@Test
    public void testFindGameByID() throws NoRecordFoundException, IDOutOfBoundException {
        Game game = new Game();

        when(gameDAO.findGameById(1)).thenReturn(game);
        Game result = gameService.findGameByID(1);
        verify(gameDAO, times(1)).findGameById(Mockito.anyInt());
        assertEquals(game, result);

    }
@Test
    public void testFindGamesMinDifficulty() throws NoRecordFoundException {
    List<Game> games = new ArrayList<>();
    games.add(new Game());

        when(gameDAO.findGameByMinimumDifficulty(1)).thenReturn(games);
        List<Game> results = gameService.findGameByDifficulty(1);
        verify(gameDAO, times(1)).findGameByMinimumDifficulty(anyInt());
        assertEquals(games, results);

}
@Test
    public void testFindAllGames() throws NoRecordFoundException {
        List<Game> games = new ArrayList<>();
        games.add(new Game());

        when(gameDAO.findAllGames()).thenReturn(games);
        List<Game> results = gameService.findAllGames();
        verify(gameDAO, times(2)).findAllGames();
        assertEquals(games, results);


    }
  /* @Test
    public void testShowGamePage() throws NoRecordFoundException {
    List<Game> games = new ArrayList<>();
    when(gameDAO.showGamesFromPage()).thenReturn(games);
    List<Game> result = gameService.showGamePage();
    verify(gameDAO, times(1)).showGamesFromPage();
    assertEquals(games,result);
    } */
@Test
    public void testTooSmallID() {
        Assertions.assertThrows(IDOutOfBoundException.class, () -> {
            Game game = gameService.findGameByID(-1);
        });
    }
@Test
    public void testNoRecordsFoundException() throws NoRecordFoundException {
    Assertions.assertThrows(NoRecordFoundException.class, () -> {
        gameService.findAllGames();

    });
}}