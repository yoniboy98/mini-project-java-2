package ServicesTest;

import Services.CategoryService;
import domain.Category;
import exceptions.NoRecordFoundException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import repo.CategoryDAO;

import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceTest {
    @Mock
    private CategoryDAO categorydao;

    @InjectMocks
    CategoryService categoryservice;

    @Test
    public void testFindCategoryById() throws NoRecordFoundException {

            Category category = new Category();
            when(categorydao.findCategoryById(1)).thenReturn(category);
            Category categorys = categoryservice.findCategoryById(1);
            Assertions.assertEquals(category, categorys);
            verify(categorydao, times(1)).findCategoryById(1);


    }

       @Test
            public void errorWhenNoRecordsAreFound() throws NoRecordFoundException {

                when(categorydao.findCategoryById(0)).thenReturn(null);
           Assertions.assertThrows(NoRecordFoundException.class, () -> {
               Category result = categoryservice.findCategoryById(0);


           });


}}




