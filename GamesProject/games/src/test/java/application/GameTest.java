package application;

import Services.CategoryService;
import domain.Category;
import exceptions.NoRecordFoundException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import repo.CategoryDAO;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class GameTest {
    @Mock
    private CategoryDAO categoryDAO;

    @InjectMocks
    CategoryService categoryService;


    @Test
    public void testCategoryFindById() throws NoRecordFoundException {
        Category category = new Category();
        when(categoryDAO.findCategoryById(1)).thenReturn(category);
        Category category1 = categoryService.findCategoryById(1);
        assertEquals(category, category1);


    }

    @Test
    public void testFindByIdThrowsNoRecordFoundException() {
        when(categoryDAO.findCategoryById(1)).thenReturn(null);
        Assertions.assertThrows(NoRecordFoundException.class, () -> {
            Category result = categoryService.findCategoryById(1);

        });
    }


}

